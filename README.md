#简介

这个项目，采用OpenResty，实现了一个令牌服务器，主要包括令牌申请、令牌延期、令牌效验、令牌删除、令牌个数查询多个接口，以HTTP+JSON提供调用


#包括几个部分

lua_scripts  用于部署到OpenResty的Lua业务脚本，实现了上面提到的接口，每一个文件包含请求报文和应答报文的示例

lualib       用于部署到OpenResty的Lua库脚本

nginx/conf/nginx.conf  OpenResty的配置文件


#安装

上面的目录都是相对于OpenResty的根目录下的子目录

把lualib里的文件放到OpenResty的lualib目录下

把lua_scripts放到OpenResty根目录下，与lualib目录在同一级

不用类型的令牌存储的配置见redis_config.lua

不同类型的令牌的同步配置见syncr_config.lua，需要参考redis_sync的配置


#依赖

[redis_sync](https://git.oschina.net/kevin158/redis_sync) 


#特性

1，支持多数据中心，通过多写来实现多数据中心间的主Redis节点的数据同步

2，Nginx直接执行Lua脚本，性能较好


#示例

1，/apply_token 令牌申请
Request body:::
{
    "service_type":"SHOPPING",
    "user_identify":"dadf233dfadf1132124",
    "at_expire_secs":1800,
    "user_member_level":"1",
    "user_term_type":"MOBILE/Andriod",
    "user_term_ip":"112.11.1.10"
}
Response body:::
{
    "status":0,
    "status_desc":"",
    "token":"eyJleHBpcmVfdGltZSI6MTQ2OTE4OTMxMSwibWVtYmVyX2xldmVsIjoiMSIsImxvZ2luX2F1dGgiOiJqYWNrIiwidmFsaWRpdHkiOjMwLCJjbGllbnRfaXAiOiIxMTIuMTEuMS4xMCIsImNsaWVudF90eXBlIjoiaXBob25lIn0="
}


2，/check_token 令牌效验
Request body:::
{
    "service_type":"SHOPPING",
    "token":"eyJleHBpcmVfdGltZSI6MTQ2OTE4OTMxMSwibWVtYmVyX2xldmVsIjoiMSIsImxvZ2luX2F1dGgiOiJqYWNrIiwidmFsaWRpdHkiOjMwLCJjbGllbnRfaXAiOiIxMTIuMTEuMS4xMCIsImNsaWVudF90eXBlIjoiaXBob25lIn0="
}
Response body:::
{
    "status":0,
    "status_desc":""
}


3，/keep_token 令牌延期
Request body:::
{
    "service_type":"SHOPPING",
    "token":"eyJleHBpcmVfdGltZSI6MTQ2OTE4OTMxMSwibWVtYmVyX2xldmVsIjoiMSIsImxvZ2luX2F1dGgiOiJqYWNrIiwidmFsaWRpdHkiOjMwLCJjbGllbnRfaXAiOiIxMTIuMTEuMS4xMCIsImNsaWVudF90eXBlIjoiaXBob25lIn0="
}
Response body:::
{
    "status":0,
    "status_desc":"",
    "token":"WyJleHBpcmVfdGltZSI6MTQ2OTE4OTMxMSwibWVtYmVyX2xldmVsIjoiMSIsImxvZ2luX2F1dGgiOiJqYWNrIiwidmFsaWRpdHkiOjMwLCJjbGllbnRfaXAiOiIxMTIuMTEuMS4xMCIsImNsaWVudF90eXBlIjoiaXBob25lIn0="
}


4，/remove_token 令牌删除
Request body:::
{
    "service_type":"SHOPPING",
    "token":"eyJleHBpcmVfdGltZSI6MTQ2OTE4OTMxMSwibWVtYmVyX2xldmVsIjoiMSIsImxvZ2luX2F1dGgiOiJqYWNrIiwidmFsaWRpdHkiOjMwLCJjbGllbnRfaXAiOiIxMTIuMTEuMS4xMCIsImNsaWVudF90eXBlIjoiaXBob25lIn0="
}
Response body:::
{
    "status":0,
    "status_desc":""
}


5，/count_token 令牌个数查询
Request body:::
{
    "service_type":"SHOPPING"
}
Response body:::
{
    "count":188,
    "status":0,
    "status_desc":""
}